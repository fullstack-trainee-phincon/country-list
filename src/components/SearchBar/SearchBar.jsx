import React from "react";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import TextField from "@mui/material/TextField";
import classes from "./SearchBar.module.scss";

const SearchBar = ({ setSearchQuery, darkMode }) => {
  const iconColor = darkMode ? "hsl(0, 0%, 100%)" : "hsl(209, 23%, 22%)";

  return (
    <form className={classes.Container}>
      <TextField
        sx={{ minWidth: 200, marginLeft: 7, marginTop: 4 }}
        id="search-bar"
        className="text"
        onInput={(e) => {
          setSearchQuery(e.target.value);
        }}
        label="Search by Country"
        variant="outlined"
        placeholder="Search..."
        size="small"
      />
      <IconButton sx={{ width: 60 }} type="submit" aria-label="search">
        <SearchIcon style={{ fill: iconColor }} />
      </IconButton>
    </form>
  );
};

export default SearchBar;
