import React from "react";
import classes from "./NavBar.module.scss";
import Button from "@mui/material/Button";
import NightlightRoundIcon from "@mui/icons-material/NightlightRound";
import WbSunnyIcon from "@mui/icons-material/WbSunny";

const NavBar = ({ darkMode, handleThemeToggle }) => {
  return (
    <div
      className={`${classes.NavBox} ${darkMode ? classes.NavBox_DarkMode : ""}`}
    >
      <div
        className={`${classes.World} ${darkMode ? classes.World_DarkMode : ""}`}
      >
        Where in the world?
      </div>
      <Button
        sx={{ scale: "70%" }}
        className={classes.Button}
        variant="contained"
        color="primary"
        startIcon={darkMode ? <WbSunnyIcon /> : <NightlightRoundIcon />}
        onClick={handleThemeToggle}
      >
        {darkMode ? "Light Mode" : "Dark Mode"}
      </Button>
    </div>
  );
};

export default NavBar;
