import { createTheme } from "@mui/material/styles";

const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#ffffff",
    },
    secondary: {
      main: "##606060",
    },
    mode: "dark",
  },
});

export default darkTheme;
