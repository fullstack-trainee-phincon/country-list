import { createTheme } from "@mui/material/styles";

const lightTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#ffffff",
    },
    mode: "light",
  },
});

export default lightTheme;
