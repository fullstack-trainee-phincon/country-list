import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";

const MediaCard = ({ countryData, handleCountryClick }) => {
  const handleClick = () => {
    handleCountryClick(countryData);
  };

  return (
    <Card sx={{ maxWidth: 300, borderRadius: "8px" }} onClick={handleClick}>
      {countryData && (
        <CardMedia
          sx={{ height: 200 }}
          image={countryData.flags.svg}
          title={countryData.name.common}
        ></CardMedia>
      )}
      <CardContent>
        {countryData && (
          <>
            <Typography gutterBottom variant="h5" component="div">
              {countryData.name.common}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Population: {countryData.population}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Region: {countryData.region}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Capital: {countryData.capital}
            </Typography>
          </>
        )}
      </CardContent>
    </Card>
  );
};

export default MediaCard;
