import React, { useEffect, useState } from "react";
import MediaCard from "../../components/MediaCard/MediaCard";
import classes from "../MediaCard/MediaCard.module.scss";
import { Link } from "react-router-dom";
import CountryDetails from "../../pages/CountryDetails/CountryDetails";

const MediaCardLogic = ({
  darkMode,
  countriesData,
  selectedRegion,
  selectedCountry,
  handleCountryClick,
  searchQuery,
  setSelectedCountry,
  sortOrder,
}) => {
  const [sortedData, setSortedData] = useState([]);

  const filterData = (query, region, data) => {
    if (!query && region === "All") {
      return data;
    } else {
      return data.filter(
        (d) =>
          (!query ||
            d.name.common.toLowerCase().includes(query.toLowerCase())) &&
          (region === "All" || d.region === region)
      );
    }
  };

  useEffect(() => {
    const handleBackToCountries = () => {
      setSelectedCountry(null);
    };

    const filteredData = filterData(searchQuery, selectedRegion, countriesData);

    // Sort the data based on sortOrder
    const sortedData = [...filteredData].sort((a, b) =>
      sortOrder === "asc"
        ? a.name.common.localeCompare(b.name.common)
        : b.name.common.localeCompare(a.name.common)
    );

    setSortedData(sortedData);
  }, [
    selectedRegion,
    searchQuery,
    countriesData,
    sortOrder,
    setSelectedCountry,
  ]);

  return (
    <>
      {selectedCountry ? (
        <CountryDetails
          countryData={selectedCountry}
          handleBackToCountries={handleBackToCountries}
        />
      ) : (
        <div
          className={`${classes.content1} ${
            darkMode ? classes.content1_DarkMode : ""
          }`}
        >
          <div className={`${classes.Content}`}>
            {sortedData.map((country) => (
              <Link
                className={classes.Link}
                to={`/country/${country.name.common}`}
                key={country.name.common}
              >
                <MediaCard
                  className={classes.Cards}
                  countryData={country}
                  handleCountryClick={handleCountryClick}
                />
              </Link>
            ))}
          </div>
        </div>
      )}
    </>
  );
};

export default MediaCardLogic;
