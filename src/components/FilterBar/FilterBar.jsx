import React from "react";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  IconButton,
} from "@mui/material";
import { SortByAlpha, SortByAlphaOutlined } from "@mui/icons-material";

const FilterBar = ({
  selectedRegion,
  handleFilterChange,
  handleSortToggle,
  sortOrder,
  handleSort, // Add handleSort to the props
}) => {
  return (
    <FormControl
      sx={{ minWidth: 200, width: 100, marginBottom: 5, marginTop: 2 }}
    >
      <InputLabel id="region-filter-label">Filter by Region</InputLabel>
      <Select
        labelId="region-filter-label"
        id="region-filter-select"
        value={selectedRegion}
        label="Filter by Region"
        onChange={handleFilterChange}
      >
        <MenuItem value="All">All</MenuItem>
        <MenuItem value="Africa">Africa</MenuItem>
        <MenuItem value="Americas">America</MenuItem>
        <MenuItem value="Asia">Asia</MenuItem>
        <MenuItem value="Europe">Europe</MenuItem>
        <MenuItem value="Oceania">Oceania</MenuItem>
        <MenuItem
          value="Ascending"
          onClick={() => {
            console.log("MenuItem Ascending Clicked");
            handleSortToggle("asc"); // Call handleSortToggle with the selected sort order
            handleSort("asc"); // Call handleSort with the selected sort order
          }}
        >
          <IconButton size="small">
            {sortOrder === "desc" ? <SortByAlphaOutlined /> : <SortByAlpha />}
          </IconButton>
          Descending
        </MenuItem>
        <MenuItem
          value="Descending"
          onClick={() => {
            console.log("MenuItem Descending Clicked");
            handleSortToggle("desc"); // Call handleSortToggle with the selected sort order
            handleSort("desc"); // Call handleSort with the selected sort order
          }}
        >
          <IconButton size="small">
            {sortOrder === "asc" ? <SortByAlphaOutlined /> : <SortByAlpha />}
          </IconButton>
          Ascending
        </MenuItem>
      </Select>
    </FormControl>
  );
};

export default FilterBar;
