import React from "react";
import PropTypes from "prop-types";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import classes from "./BackButton.module.scss";

const BackButton = ({ darkTheme, onBack }) => (
  <div className={classes.backButton} onClick={onBack}>
    <img
      className={classes.icon}
      src={darkTheme ? <ArrowBackIcon /> : <ArrowBackIcon />}
      alt=""
    />
    {/* {en.back} */}
  </div>
);

BackButton.propTypes = {
  darkTheme: PropTypes.bool,
  onBack: PropTypes.func,
};

export default BackButton;
