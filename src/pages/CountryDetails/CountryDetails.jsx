import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";
import classes from "./CountryDetails.module.scss";
import { useNavigate } from "react-router-dom";
import { useTheme } from "@mui/material/styles";

const CountryPage = ({ darkMode }) => {
  const { name } = useParams();
  const [countryDetails, setCountryDetails] = useState(null);
  const [showBackButton, setShowBackButton] = useState(false);

  useEffect(() => {
    axios
      .get(`https://restcountries.com/v3.1/name/${name}`)
      .then((response) => {
        const countryData = response.data[0];
        setCountryDetails(countryData);
        setShowBackButton(true);
      })
      .catch((error) => {
        console.error("Error fetching country details:", error);
      });
  }, [name]);

  const navigate = useNavigate();

  const handleBack = () => {
    navigate.goBack();
  };

  if (!countryDetails) {
    return (
      <div className={classes.ldsRollerBox}>
        <div className={classes.ldsRoller}>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    );
  }

  const {
    capital,
    tld,
    currencies,
    languages,
    population,
    region,
    subregion,
    borders,
    name: countryName,
    flags,
  } = countryDetails;

  return (
    <div
      className={`${classes.ContainerCP} ${
        darkMode ? classes.ContainerCP_DarkMode : ""
      }`}
    >
      <div
        className={`${classes.BackBoxButton} ${
          darkMode ? classes.BackBoxButton_DarkMode : ""
        }`}
      >
        {showBackButton && (
          <Link to="/" onClick={handleBack}>
            <button
              className={`${classes.ButtonBack} ${
                darkMode ? classes.ButtonBack_DarkMode : ""
              }`}
            >
              Back
            </button>
          </Link>
        )}
      </div>

      <div
        className={`${classes.countryDetailsContainer} ${
          darkMode ? classes.countryDetailsContainer_DarkMode : ""
        }`}
      >
        <div className={classes.LeftContainer}>
          <img src={flags.svg} alt={countryName.common} />
        </div>
        <div className={classes.RightContainer}>
          <h2>{countryName.common}</h2>

          <div className={classes.MiddleBox}>
            <div className={classes.LeftDesc}>
              <p>
                <strong>Population:</strong> {population}
              </p>
              <p>
                <strong>Region:</strong> {region}
              </p>
              <p>
                <strong>Sub Region:</strong> {subregion}
              </p>
              <p>
                <strong>Capital:</strong> {capital && capital[0]}
              </p>
            </div>

            <div className={classes.RightDesc}>
              {tld && (
                <p>
                  <strong>Top-Level Domain:</strong> {tld.join(", ")}
                </p>
              )}
              {currencies && (
                <p>
                  <strong>Currencies:</strong>{" "}
                  {Object.keys(currencies)
                    .map(
                      (currency) =>
                        `${currencies[currency].name} (${currencies[currency].symbol})`
                    )
                    .join(", ")}
                </p>
              )}
              {languages && (
                <p>
                  <strong>Languages:</strong>{" "}
                  {Object.keys(languages).join(", ")}
                </p>
              )}
            </div>
          </div>

          {borders && (
            <p>
              <strong>Border Countries:</strong> {borders.join(", ")}
            </p>
          )}
        </div>
      </div>
    </div>
  );
};

export default CountryPage;
