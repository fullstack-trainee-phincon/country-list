import React, { useState, useEffect } from "react";
import classes from "../App/App.module.scss";
import SearchBar from "../../components/SearchBar/SearchBar";
import FilterBar from "../../components/FilterBar/FilterBar";
import NavBar from "../../components/NavBar/NavBar";
import MediaCardLogic from "../../components/MediaCard/MediaCardLogic";
import axios from "axios";
import { ThemeProvider } from "@mui/material/styles";
import lightTheme from "../../components/Themes/LightThemes";
import darkTheme from "../../components/Themes/DarkThemes";
import { Routes, Route, Link } from "react-router-dom";
import CountryPage from "../CountryDetails/CountryDetails";
import BackButton from "../../components/BackButton/BackButton";

function App() {
  const [countriesData, setCountriesData] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("All");
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [darkMode, setDarkMode] = useState(false);
  const [sortOrder, setSortOrder] = useState("asc");

  const handleFilterChange = (event) => {
    setSelectedRegion(event.target.value);
  };

  const handleSortToggle = (order) => {
    console.log("Sorting Order:", order);
    setSortOrder(order);
    console.log("Updated Sort Order:", sortOrder);
  };

  useEffect(() => {
    axios
      .get("https://restcountries.com/v3.1/all")
      .then((response) => {
        setCountriesData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching country data:", error);
      });
  }, []);

  useEffect(() => {
    document.body.style.backgroundColor = darkMode
      ? "hsl(207, 26%, 17%)"
      : "#fff";
  }, [darkMode]);

  const handleCountryClick = (country) => {
    setSelectedCountry(country);
  };

  return (
    <>
      <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
        <NavBar
          darkMode={darkMode}
          handleThemeToggle={() => setDarkMode((prevDarkMode) => !prevDarkMode)}
        />

        <Routes>
          <Route
            path="/"
            element={
              <>
                <div
                  className={`${classes.FilterSearchContainer} ${
                    darkMode ? classes.FilterSearchContainer_DarkMode : ""
                  }`}
                >
                  <SearchBar setSearchQuery={setSearchQuery} />
                  <FilterBar
                    selectedRegion={selectedRegion}
                    handleFilterChange={handleFilterChange}
                    handleSortToggle={() =>
                      setSortOrder((prevSortOrder) =>
                        prevSortOrder === "asc" ? "desc" : "asc"
                      )
                    }
                    sortOrder={sortOrder}
                  />
                </div>
                <div className={classes.MediaCard}>
                  <MediaCardLogic
                    darkMode={darkMode}
                    countriesData={countriesData}
                    selectedRegion={selectedRegion}
                    selectedCountry={selectedCountry}
                    handleCountryClick={handleCountryClick}
                    searchQuery={searchQuery}
                    setSelectedCountry={setSelectedCountry}
                    sortOrder={sortOrder}
                  />
                </div>
              </>
            }
          />
          <Route
            path="/country/:name"
            element={
              <>
                <BackButton />
                <CountryPage darkMode={darkMode} />
              </>
            }
          />
        </Routes>
      </ThemeProvider>
    </>
  );
}

export default App;
